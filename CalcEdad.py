from mblock import event
from pygame import sprite

@event.greenflag
def on_greenflag():
    sprite.say('¡Hola! Me llamo Carmelo el Oso', 2)
    sprite.say('y hoy intentaremos adivinar tu edad', 3)
    sprite.say('pero primero, necesito saber cómo te llamas', 3.3)
    answer = sprite.input('¿Como te llamas?')
    sprite.say(str('Hola ') + str(sprite.answer), 2)
    answer = sprite.input('¿Cuál es tu año de nacimiento?')
    sprite.think('Dejame pensarlo por unos segundos',5)
    sprite.set_variable('edad', ((sprite.datetime.year - sprite.answer)))
    sprite.think(str('Tu edad es ') + str(str(sprite.get_variable('edad')) + str(' años')),5)
